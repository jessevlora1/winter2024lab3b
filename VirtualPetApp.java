import java.util.Scanner;
public class VirtualPetApp{
	public static void main (String[] args){
		Jaguar[] prowl = new Jaguar[4];
		
		Scanner reader = new Scanner(System.in);
		
		for(int i = 0; i < prowl.length; i++){
			prowl[i] = new Jaguar();
			System.out.println("How fast is your jaguar?");
			prowl[i].speed = Integer.parseInt(reader.nextLine());
			System.out.println("What is the name of your jaguar?");
			prowl[i].name = reader.nextLine();
			System.out.println("How much does your jaguar weigh?");
			prowl[i].weight = Integer.parseInt(reader.nextLine());
		}
		System.out.println(prowl[prowl.length -1].speed);
		System.out.println(prowl[prowl.length -1].name);
		System.out.println(prowl[prowl.length -1].weight);
		
		
		prowl[0].isFast();
		prowl[0].isFit();
		
	}

}